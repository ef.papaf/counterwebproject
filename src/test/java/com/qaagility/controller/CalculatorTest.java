package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Test
    public void testAdd() {
        // Arrange
                Calculator calculator = new Calculator();
       
              // Act
                int result = calculator.add();
       
                                        // Assert
              assertEquals(9, result);
                    }
            }
